# SSH_Links

## SSh General:

* <https://infosec.mozilla.org/guidelines/openssh#recommended-safer-alternatives-to-ssh-agent-forwarding>
* <https://fedoramagazine.org/using-ansible-to-organize-your-ssh-keys-in-aws/>
* <https://www.redhat.com/sysadmin/secure-session-recording>
* <https://www.redhat.com/sysadmin/session-recording-tlog>
* <https://www.redhat.com/sysadmin/basic-concepts-encryption-cryptography>
* <https://fedoramagazine.org/network-bound-disk-encryption-with-stratis/>
* <https://robotmoon.com/ssh-tunnels/>
* <https://www.vultr.com/docs/how-do-i-generate-ssh-keys>
* <https://www.vultr.com/docs/using-your-ssh-key-to-login-to-non-root-users>
* <https://www.vultr.com/docs/how-to-use-twofactor-authentication-with-ubuntu-20-04>
* <https://www.vultr.com/docs/how-to-configure-encrypted-ftp-for-your-web-server>
* <https://www.vultr.com/docs/how-to-add-and-delete-ssh-keys>
* <https://www.linode.com/docs/guides/use-public-key-authentication-with-ssh/>
* <https://www.linode.com/docs/guides/how-to-use-fail2ban-for-ssh-brute-force-protection/>
* <https://www.linode.com/docs/guides/using-sshfs-on-linux/>
* <https://infosec.mozilla.org/guidelines/openssh>
* <https://linux-audit.com/audit-and-harden-your-ssh-configuration/#ssh-basics>

## SSH Certificate Authority

* <https://keybase.io/blog/keybase-ssh-ca>
* <https://github.com/keybase/bot-sshca>
* <https://docs.github.com/en/github/setting-up-and-managing-organizations-and-teams/about-ssh-certificate-authorities>
* <https://docs.github.com/en/github/setting-up-and-managing-organizations-and-teams/managing-your-organizations-ssh-certificate-authorities>
* <https://github.com/cloudtools/ssh-cert-authority>
* <https://medium.com/better-programming/how-to-use-ssh-certificates-for-scalable-secure-and-more-transparent-server-access-720a87af6617>
* <https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/sec-using_openssh_certificate_authentication>
* <https://ibug.io/blog/2019/12/manage-servers-with-ssh-ca/>
* <https://gist.github.com/kawsark/a9443692a9e4a7b1c7df253995ce864c>
