# GameDev_Links

# GoDot:

<https://github.com/godotengine/awesome-godot>

<https://github.com/GDQuest/godot-shaders>

<https://github.com/Zylann/godot_voxel>

## GDScript:

<https://github.com/GDQuest/learn-gdscript>

<https://gdquest.github.io/learn-gdscript/> (Requires WebGL.)

<https://github.com/habamax/vim-godot>

<https://github.com/touilleMan/godot-python>

<https://godottutorials.com/courses/introduction-to-gdscript/>

## GD Plugins:

<https://github.com/QodotPlugin/qodot-plugin>

<https://github.com/PunchablePlushie/godot_ggs>

<https://github.com/quentincaffeino/godot-console>

<https://github.com/efornara/frt>

<https://github.com/uralys/fox>

## GD Demos:

<https://github.com/GDQuest/godot-demos>

<https://github.com/godotengine/godot-demo-projects>

## GD CI/CD:

<https://github.com/jason-h-35/godot-ci-template>

<https://github.com/godotengine/godot-git-plugin>

<https://github.com/bitwes/Gut>

## HN Gossip:

### Godot 4.0 Feature Freeze:

-   <https://news.ycombinator.com/item?id=32265429>

-   <https://godotengine.org/article/godot-4-0-development-enters-feature-freeze>

### Godot 4.0 Vulkan:

-   <https://news.ycombinator.com/item?id=27873511>

-   <https://godotengine.org/article/about-godot4-vulkan-gles3-and-gles2>

### Godot 3.3 Release:

-   <https://news.ycombinator.com/item?id=26896340>

-   <https://godotengine.org/article/godot-3-3-has-arrived>

* * *

# Assets

## Graphics

<https://mrmotarius.itch.io/mrmo-halftone>

<https://github.com/lettier/3d-game-shaders-for-beginners>

<https://github.com/lettier/GameDev-Resources>

<https://github.com/ellisonleao/magictools>

<https://github.com/love2d-community/awesome-love2d>

<https://github.com/sehugg/awesome-8bitgamedev>

<https://github.com/sehugg/8bit-tools/>

<https://libresprite.github.io/#!/>

<https://github.com/luisnts/awesome-computer-graphics>

<https://nostarch.com/computer-graphics-scratch>

<https://docs.spline.design/>

<https://github.com/goabstract/Awesome-Design-Tools>

# 8bitworkshop

<https://github.com/sehugg/8bitworkshop>

<https://sr.ht/~rabbits/famicom-cookbook/>

<http://www.pikuma.com/>


## UI

<https://www.gameuidatabase.com/gameData.php?id=801>

## Fonts

<https://github.com/robhagemans/hoard-of-bitfonts>

<https://github.com/emilk/egui>

## Misc Graphics

<https://aeriform.itch.io/minicube64>

<https://aeriform.gitbook.io/minicube64/>

<https://skilldrick.github.io/easy6502>

<https://github.com/henriquelalves/SimpleGodotCRTShader>

<https://github.com/utilForever/game-developer-roadmap>

<https://opengameart.org/>

<https://magnum.graphics/>

<https://pixabay.com/>

## Audio

<https://pixabay.com/music/>

<https://tenacityaudio.org/>

* * *

# Source Code, Ports, Reverse Engineering:

## ChipsChallenge

<https://github.com/eevee/lexys-labyrinth>

<https://bitbusters.club/>

<https://wiki.bitbusters.club/Main_Page>

## Jak & Daxter:

<https://github.com/open-goal/jak-project>
<https://github.com/water111/jak-project>

## Skii Free

<https://twitter.com/Foone/status/1536053690368348160>
<https://ski.ihoc.net/>

## Nethack

<https://github.com/NetHack/NetHack>

<https://arstechnica.com/gaming/2020/03/ascii-art-permadeath-the-history-of-roguelike-games/>

<https://everything2.com/title/You+have+a+sad+feeling+for+a+moment%252C+then+it+passes>

## JediKnight

<https://github.com/grayj/Jedi-Academy>

<https://github.com/grayj/Jedi-Outcast>

<https://github.com/JACoders/OpenJK>

## Doom/Quake

<https://github.com/TTimo/doom3.gpl>

<https://github.com/id-Software>

<https://github.com/id-Software/DOOM-3>

<https://github.com/id-Software/Quake-2>

## Misc

<https://github.com/blinry/gish>

<https://github.com/endless-sky/endless-sky>

<https://github.com/jmechner/Prince-of-Persia-Apple-II>

## duke2opengl

<https://github.com/lethal-guitar/RigelEngine>

## N64 Development

<https://github.com/command-tab/awesome-n64-development>

### LoZ Decompilation / Source:

<https://github.com/TinyRetroWarehouse/tp>

<https://github.com/TinyRetroWarehouse/mm>

<https://github.com/TinyRetroWarehouse/oot>

<https://github.com/zeldaret/botw>

<https://gbatemp.net/threads/super-mario-64s-source-code-has-been-decompiled-and-officially-released.546814/>

<https://gbatemp.net/threads/ocarina-of-time-fan-pc-port-expected-to-release-in-april.606783/>

### SSB64:

<https://github.com/sm64-port/sm64-port>

<https://github.com/sm64pc/sm64ex>

## Text-Based-Adventures

<https://if50.substack.com/p/1984-the-hitchhikers-guide-to-the-galaxy>

* * *

# Hundred Rabbits

<https://hundredrabbits.itch.io/>

<https://git.sr.ht/~rabbits/>

<https://100r.co/site/resources.html>

<https://wiki.xxiivv.com/site/studio.html>

<https://wiki.xxiivv.com/site/hardware.html>

* * *

# Misc Engines:

## Torque2D:

<https://github.com/TorqueGameEngines/Torque2D>

## Torque3D:

<https://github.com/TorqueGameEngines/Torque3D>

## Gdevelop:

<https://gdevelop.io/download>

<https://github.com/4ian/GDevelop>

## Defold

<https://defold.com/download/>

<https://github.com/defold>

<https://github.com/defold/awesome-defold>

<https://defold.com/assets/>

## Pixel:

<https://github.com/faiface/pixel>

# Quake

<https://quakeengines.github.io/#>

## ID Tech 3
<https://en.wikipedia.org/wiki/Id_Tech_3>

<https://github.com/ioquake/ioq3>

<https://www.ioquake3.org/>

# Quake Engine written in RUST:
<https://github.com/cormac-obrien/richter>

# Quake3 Lite:
<https://github.com/cdev-tux/q3lite>

# VitaQuake:
<https://github.com/Rinnegatamante/vitaQuake>

# Blender support for MD3:
<https://github.com/neumond/blender-md3>

# Map Editor:
<https://github.com/Garux/netradiant-custom>

<https://garux.github.io/NRC/>

* * *

## Mods

* * *

    steamcommunity.com/sharedfiles/filedetails/?id=123626484
    steamcommunity.com/sharedfiles/filedetails/?id=732999296
    moddb.com/games/quake/tutorials/secondary-trigger-in-quake
    www.quaddicted.com/engines/software_vs_glquake
    www.quaddicted.com/quake/configuration
    ezquake.sourceforge.net/docs/?vars-texture-settings
    neogeographica.com/site/pages/tools/quakestarter.html
    web.archive.org/web/20070808003904/collective.valve-erc.com/index.php?doc=1046121871-81619100
    nextleveldesign.org/index.php?/forums/topic/82-trenchbroom-tutorials/
    valvedev.info/guides/accelerating-map-compiles-in-quake-based-engines/
    learnleveldesign.com/design-theory/theory-and-concepts/
    celephais.net/
    disenchant.net/
    quaketerminus.com/
    campu.org/
    markshan.com/
    ihoc.net/quake_archive/servers.html
    runequake.com/
    renep.home.xs4all.nl/quakeme/
    www.tabun.nl/
    mpqarchive.pauked.com/
    secretlevelsite.wordpress.com/
    zeliepa.net/
    www.quake-info-pool.net/q1/bugstool.htm
    fdossena.com/?p=quakeFluids/i.md
    quakeulf.suxos.org/3d/maps/

### Q3

* * *

    quakewiki.net/archives/code3arena/tutorials/tutorial1.shtml
    maps.rcmd.org/tutorials/q3a_create_skybox/
    www.kbs.twi.tudelft.nl/
    justinmeiners.github.io/shamans/papers/ai/quake_3_arena_bot.pdf
    discourse.ioquake.org/t/learning-quake3-modding/1384
    discourse.ioquake.org/t/ioquake3-where-do-i-get-started-dev/1369/3
    web.archive.org/web/20170302085104/q3map2.everyonelookbusy.net/shader_manual/contents.htm
    web.archive.org/web/20050211040925/www.4thdimented.com/dimented_tutorialQuake3_1.htm
    web.archive.org/web/20051227032253/www.planetquake.com/qdevels/quake2/28_1_98l.html

### QC

* * *

    steamcommunity.com/app/2310/discussions/0/1760230157504423368/
    web.archive.org/web/20001009214502/www.planetquake.com/minion/tutor.htm
    pnahratow.github.io/creating-models-for-quake-1.html
    www.gamers.org/dEngine/quake/
    quakewiki.org/wiki/QuakeC_basics
    www.quake-info-pool.net/q1/quakec.htm
    www.quakewiki.net/quakesrc/
    pages.cs.wisc.edu/~jeremyp/quake/quakec/quakec.pdf
    www.cataboligne.org/extra/qcmanual.html
